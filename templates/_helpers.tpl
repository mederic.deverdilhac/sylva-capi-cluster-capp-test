{{/*
Expand the name of the chart.
*/}}
{{- define "sylva-capi-cluster.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "sylva-capi-cluster.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "sylva-capi-cluster.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "sylva-capi-cluster.labels" -}}
helm.sh/chart: {{ include "sylva-capi-cluster.chart" . }}
{{ include "sylva-capi-cluster.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "sylva-capi-cluster.selectorLabels" -}}
app.kubernetes.io/name: {{ include "sylva-capi-cluster.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}


{{- define "containerd-config.toml-registry-config" }}
c=/etc/containerd/config.toml

# Remove default mirroring configuration for k8s.gcr.io as it can't coexist with registry config dir
sed -i '/k8s.gcr.io/d' $c
if ! grep -q "config_path *=.*/etc/containerd/registry.d" $c; then
  cp $c $c.orig
  if ! grep -q  '"io.containerd.grpc.v1.cri".registry\]' $c ; then
    # we add the missing section
    echo '[plugins."io.containerd.grpc.v1.cri".registry]' >> $c
    echo '  config_path = "/etc/containerd/registry.d"' >> $c
  else
    # we assume that it's a recent containerd config file, already having config_path set for this section
    sed -i -e '/io.containerd.grpc.v1.cri".registry\]/ { n; s|config_path.*|config_path = "/etc/containerd/registry.d"| }' $c
  fi
fi
{{ end -}}

{{/*
Get Metal3MachineTemplate.spec and return it after replacing the URL host part with an empty string.
For determining Metal3MachineTemplate.metadata.name we need to compute the hash of Metal3MachineTemplate.spec content
without the IP/hostname part in m3mt.spec.template.spec.image.checksum & m3mt.spec.template.spec.image.url,
to avoid useless node rolling upgrade after pivot, see https://gitlab.com/sylva-projects/sylva-core/-/issues/621
*/}}
{{- define "Metal3MachineTemplateSpec-remove-url-hostname" }}
{{- $m3mt_spec := fromYaml . -}}
{{- $m3mt_image_checksum := $m3mt_spec.template.spec.image.checksum -}}
{{- if (hasPrefix "http" $m3mt_spec.template.spec.image.checksum) -}}
  {{- $m3mt_image_checksum = urlParse $m3mt_spec.template.spec.image.checksum -}}
  {{- $_ := set $m3mt_image_checksum "host" "" -}}
  {{- $m3mt_image_checksum = urlJoin $m3mt_image_checksum -}}
{{- end -}}
{{- $m3mt_image_url := urlParse $m3mt_spec.template.spec.image.url -}}
{{- $_ := set $m3mt_image_url "host" "" -}}
{{- $m3mt_image_url = urlJoin $m3mt_image_url -}}
{{- $m3mt_spec_replaced := mergeOverwrite $m3mt_spec (dict "template" (dict "spec" (dict "image" (dict "checksum" $m3mt_image_checksum
                                                                                                       "url"      $m3mt_image_url))))
-}}
{{- $m3mt_spec_replaced | toYaml -}}
{{- end -}}


{{- define "shell-longhorn-mounts" -}}
# handle longhorn disk mounts defined via Metal3DataTemplate based on BaremetalHost annotations
default_disk="{{`{{ds.meta_data.disk}}`}}"
sylva_longhorn_disks="{{`{{ds.meta_data.sylva_longhorn_disks}}`}}"

if [ -n "$default_disk" ] ; then
  echo "processing Lonhorn disk mount defined by 'disk' annotation"

  # partition disk if not already partitioned
  if [ -z "$(lsblk "$default_disk" -o FSTYPE -n)" ]; then
    mkfs.ext4 "$default_disk"
  else
    echo "$default_disk is not empty:"
    lsblk "$default_disk"
  fi

  mkdir -p /var/lib/longhorn
  echo "$default_disk /var/lib/longhorn ext4 defaults 0 0" >> /etc/fstab
  mount --target /var/lib/longhorn
fi

if [ -n "$sylva_longhorn_disks" ] ; then
  echo "processing Longhorn disk mounts defined by sylvaproject.org/longhorn-disks annotation"
  for disk in $(echo "$sylva_longhorn_disks" | tr ',' ' ') ; do
    dev=/dev/$disk
    mnt=/var/lib/longhorn-$(echo "$disk" | tr / _)
    echo "longhorn disk $dev -> $mnt"

    # partition disk if not already partitioned
    if [ -z "$(lsblk "$dev" -o FSTYPE -n)" ]; then
      mkfs.ext4 $dev
    else
      echo "$dev is not empty:"
      lsblk "$dev"
    fi

    mkdir -p $mnt
    echo "$dev $mnt ext4 defaults 0 0" >> /etc/fstab
    mount --target $mnt
  done
fi
{{- end -}}


{{/*
merge-append template: like mergeOverwrite, but append lists instead of overwriting them.

Usage:

    tuple $dst $src | include "merge-append"

Values:

    dest:
      one: thing
      two:
       - a
       - b

    source1:
      one: stuff
      two:
       - a
       - c

    source2:
      one: other
      two:
       - d

Template:

{{- tuple .Values.dest .Values.source1 .Values.source2 | include "merge-append" }}
{{ .Values.dest | toYaml }}

Result:

    one: other
    two:
    - a
    - b
    - a
    - c
    - d

*/}}
{{- define "merge-append" }}
    {{- $dst := first . }}
    {{- range $src := rest . }}
        {{- $dst := tuple $dst $src | include "_merge-append" }}
    {{- end }}
{{- end }}

{{- define "_merge-append" }}
    {{- $dst := index . 0 }}
    {{- $src := index . 1 }}
    {{- if not (kindIs "map" $src) }}
        {{- fail (printf "'merge-append' called with a source object which is not a map (<%s> is %s)" ($src | toString) (kindOf $src)) }}
    {{- end }}
    {{- if not (kindIs "map" $dst) }}
        {{- fail (printf "'merge-append' called with a source object which is not a map (<%s> is %s)" ($dst | toString) (kindOf $dst)) }}
    {{- end }}
    {{- $result := $dst }}
    {{- range $key,$value := $src }}
        {{- if (hasKey $dst $key) }}
            {{- if (eq (kindOf $value) (kindOf (get $dst $key))) }}
                {{- if (eq (kindOf $value) "slice") }}
                    {{- /* append src list to dst one */}}
                    {{- $_ := set $result $key (concat (get $dst $key) $value) }}
                {{- else if (eq (kindOf $value) "map") }}
                    {{- /* recurse in that case */}}
                    {{- $_ := tuple (get $dst $key) $value | include "merge-append" }}
                {{- else }}
                    {{- /* we can't merge other kind of values like string or integers, overwrite dst value with src one */}}
                    {{- /* NOTE: maybe we should have a merge-append and merge-overwrite-append to choose whether we should keep value from dst or not */}}
                    {{- $_ := set $result $key $value }}
                {{- end }}
            {{- else }}
                {{- /* we can't merge two different kind of values, overwrite dst value with src one */}}
                {{- /* NOTE: maybe we should have a must-merge-append variant that would fail in that case? of fail inconditionally? */}}
                {{- $_ := set $result $key $value }}
            {{- end }}
        {{- else }}
            {{- /* key not in dst, merge it from src */}}
            {{- $_ := set $result $key $value }}
        {{- end }}
    {{- end }}
    {{- $dst := $result }}
{{- end }}

{{/*
Define rke2aliascommands as a list of commands.
*/}}
{{- define "rke2aliascommands" -}}
- echo 'alias ctr="/var/lib/rancher/rke2/bin/ctr --namespace k8s.io --address /run/k3s/containerd/containerd.sock"' >> /root/.bashrc
- echo 'alias crictl="/var/lib/rancher/rke2/bin/crictl --runtime-endpoint /run/k3s/containerd/containerd.sock"' >> /root/.bashrc
- echo 'alias kubectl="KUBECONFIG=/var/lib/rancher/rke2/agent/kubelet.kubeconfig /var/lib/rancher/rke2/bin/kubectl"' >> /root/.bashrc
{{- end }}

{{/*
Check the Linux distributor is openSUSE and configure DNS in /etc/sysconfig/network/config_path.

Usage:

    tuple $assign_dns_from_dhcp $dns_server | include "shell-opensuse-dns"

- whenever $assign_dns_from_dhcp is equal to string "true", then only "netconfig update -f" is used, while NETCONFIG_DNS_POLICY is set to the (default) value "auto"; see https://gitlab.com/sylva-projects/sylva-elements/helm-charts/sylva-capi-cluster/-/merge_requests/199#note_1677434793,
- whenever $assign_dns_from_dhcp is not equal to string "true", then NETCONFIG_DNS_STATIC_SERVERS and NETCONFIG_DNS_POLICY are set to the values of $dns_server and "STATIC", respectively.
*/}}
{{- define "shell-opensuse-dns" -}}
{{- $assign_dns_from_dhcp := index . 0 -}}
{{- $dns_server:= index . 1 -}}

OS_DISTRIBUTOR=$(lsb_release -i | awk -F ' ' '{print $3}')
if [[ "${OS_DISTRIBUTOR}" == "openSUSE" ]]; then
  ASSIGN_DNS_FROM_DHCP={{ $assign_dns_from_dhcp }}
  DNS_STATIC_SERVERS={{ $dns_server }}
  DNS_POLICY="auto"
  if [[ ! "${ASSIGN_DNS_FROM_DHCP}" == "true" ]]; then
    DNS_POLICY="STATIC"
    sed -i "s/NETCONFIG_DNS_STATIC_SERVERS=.*/NETCONFIG_DNS_STATIC_SERVERS=${DNS_STATIC_SERVERS}/" /etc/sysconfig/network/config
  fi
    sed -i "s/NETCONFIG_DNS_POLICY=.*/NETCONFIG_DNS_POLICY=${DNS_POLICY}/" /etc/sysconfig/network/config
    netconfig update -f
fi
{{- end -}}
